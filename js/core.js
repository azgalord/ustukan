var start, next, ok, refresh, background, step1, itemFood, person, currentFood, activeStep, ifOk, currentPerson;

function init() {
  start = $('.bashto');
  next = $('.kiyinki');
  ok = $('.buttu');
  refresh = $('.refresh');
  background = $('.background');
  step1 = $('.step1');
  itemFood = $('.item-food');
  person = $('.pers');
  currentFood = null;
  ifOk = true;

  itemFood.draggable({
    drag: function () {
      currentFood = $(this);
      var persActive = $('.persons-active');
      var foodItemsLength = persActive.find('.item-food').length;
      var lengthOfStep = persActive.find('.pers').length;

      // if (currentFood.parents('.pers').length !== 0) {
      //   currentPerson = currentFood.parents('.pers');
      //   currentFood.parents('.pers').addClass('active-pers');
      // }
      if (lengthOfStep === foodItemsLength) {
        itemFood.css({'position': 'static'});
      }
    }
  });

  person.droppable({
    drop: function (event, ui) {
      var persActive = $('.persons-active');
      var foodItemsLength = persActive.find('.item-food').length;
      var lengthOfStep = persActive.find('.pers').length;
      var hasItem = $(this).find('.item-food');

      if (lengthOfStep !== foodItemsLength && hasItem.length === 0) {
        currentFood.addClass('item-food-active');
        $(this).append(currentFood);

        foodItemsLength = persActive.find('.item-food').length;
        lengthOfStep = persActive.find('.pers').length;
        console.log(lengthOfStep, ', ', foodItemsLength);

        if (lengthOfStep === foodItemsLength) {
          ok.css({'display': 'block'});
        }
      } else {
        currentFood.css({'top' : 0, 'left': 0});
      }
    }
  });

  $('.food').droppable({
    drop: function () {
      currentFood.css({'top' : 0, 'left': 0});
    }
  });

  $('.persons').droppable({
    drop: function (event) {
      event.stopPropagation();
      currentFood.css({'top' : 0, 'left': 0});
    }
  });
}

init();

var whiteBg = $('.white-bg');
var beggingLogo = $('.begging-logo');
var begging = $('.begging');
var toybossLogo = $('.toyboss-logo');
var sertificate = $('.sertificate');
var social = $('.social');
var nowLvl = 1;
var allStepRules = [
  ['02', '01'],
  ['04', '03', '02', '01', '05', '07'],
  ['05', '07', '07', '03', '02', '01', '04', '03', '04', '05'],
  ['06', '03', '03', '02', '01', '02', '04', '07', '04', '05'],
  ['03', '05', '06', '06', '01', '02'],
];

var allStepErrors = ['Аялдардын улуусуна тартылат. Эркек кишиге тартылбайт', 'Меймандардын аксакалына тартылат', 'Улуктугу жагынан экинчи турган мейманга тартылат',
  'Улуктугу жагынан үчүнчү турган устукан. Аялга да эркекке да тартылчу устукан', 'Эркек кишиге гана тартылчу устукан', 'Кичүү меймандарга же башка устукандарга кошумча тартылышы мүмкүн',
  'Кашка жиликтен кийинки турган устукан', 'Келиндерге, көп учурда кичүүсүнө берүлчү устукан, эркек жебейт'];

function getStepToMonitor(number) {
  var img = $('<img width="200" alt="">').attr('src', "img/svg/dengel-" + number + ".svg");
  var step = $('<div class="dengel"></div>').append(img);

  background.append(step);

  setTimeout(function () {
    step.css({'opacity': 1, 'transition': '1s', 'top': '24%'});
  }, 0);

  setTimeout(function () {
    step.css({'opacity': 0});
  }, 1500);
}

function toggleClasses(step) {
  step.toggleClass('active');
  step.find('.persons').toggleClass('persons-active');
  activeStep = $('.active').html();
}

function checkIfNextStep(stepRules, stepErrors) {
  var foodItems = $('.persons-active').find('.item-food');

  for (var i = 0; i < stepRules.length; i++) {
    var dataText = foodItems.eq(i).attr('data-text');
    var dataTextNumber = parseInt(dataText);

    var checkFirstPerson = (nowLvl === 4 && i === 0 && (dataText === stepRules[i] || dataText === '07'));
    var checkSecondPerson = (nowLvl === 4 && i === 4 && (dataText === stepRules[i] || dataText === '06'));
    var checkThirdPerson = (nowLvl === 4 && i === 9 && (dataText === stepRules[i] || dataText === '06'));
    var checkForthPerson = (nowLvl === 4 && i === 7 && (dataText === stepRules[i] || dataText === '05'));

    if (checkFirstPerson || checkSecondPerson || checkThirdPerson || checkForthPerson) {
      console.log('1');
    } else if (dataText !== stepRules[i]) {
      var thisPersons = $('.persons-active');
      var thisPerson = thisPersons.find('.pers').eq(i);
      var thisImage = thisPerson.find('.pers-item');
      var currentAttrOfImg = thisImage.attr('data-text');
      var currentAttrOfPersons = thisPersons.attr('data-text');
      var errorIcon = $('<img class="wrapper-error" src="img/error_icon.svg" alt=""/>');

      var errorText = $('<p></p>').html(stepErrors[dataTextNumber - 1]);
      var error = $('<div class="meats-error-text"><img src="img/info.svg" alt=""></div>').append(errorText);

      thisImage.attr('src', 'img/character-' + 'error' + '/' + currentAttrOfPersons + '/ch_' + currentAttrOfImg + '.svg');
      ifOk = false;

      var activeFood = $('.active');

      refresh.css({'display': 'block'});
      ok.css({'display' : 'none'});
      $(thisPerson.append(error)).prepend(errorIcon);
      activeFood.find('.food').find('.item-food').css({'display': 'none'});
      activeFood.find('.food').addClass('food-bottom');
      thisPersons.addClass('persons-bottom');
    }
  }

  ok.css({'display': 'none'});

  if (ifOk) {
    next.css({'display': 'block'});
    itemFood.css({'position': 'relative'});
  }

  if (nowLvl === 5 && ifOk) {
    $('.active').css({'display': 'none'});
    next.css({'display': 'none'});
    sertificate.css({'display': 'block'});
    social.css({'display': 'flex'});
    refresh.css({'display' : 'none'});
  }
}

refresh.on('click', function () {
  var activeFood = $('.active');

  activeFood.remove();
  $('.background').append($('<div></div>').attr('class', 'step' + nowLvl + ' active').append(activeStep));
  $(this).css({'display': 'none'});
  ok.css({'display': 'none'});

  init();
});

start.on('click', function () {
  start.css({'display': 'none'});
  whiteBg.css({'display': 'none'});
  beggingLogo.css({'display': 'none'});
  begging.css({'display': 'none'});
  toybossLogo.css({'display': 'block'});

  getStepToMonitor('01');
  setTimeout(function () {
    toggleClasses(step1);
  }, 2000);
});

ok.on('click', function () {
  checkIfNextStep(allStepRules[nowLvl - 1], allStepErrors)
});

// 2-step

next.on('click', function () {
  ifOk = true;
  toggleClasses($('.step' + nowLvl));
  nowLvl++;
  getStepToMonitor('0' + nowLvl);
  setTimeout(function () {
    toggleClasses($('.step' + nowLvl));
  }, 2000);
  next.css({'display': 'none'});
});
